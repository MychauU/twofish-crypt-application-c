﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Reflection;

namespace BSK_twofish
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        internal BindingList<PublicUser> publicUsers;
        internal BindingList<PrivateUser> privateUsers;
        BackgroundWorker backgroundWorker = null;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryControl.createDirectory();
            publicUsers = new BindingList<PublicUser>();
            publicUsers.ListChanged += PublicUsers_ListChanged;
            privateUsers = new BindingList<PrivateUser>();
            this.cryptBlockLengthBox.Enabled = false;
            loadUsers();
        }

        

        private void loadUsers()
        {
            backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress = true;
            //backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.DoWork += new DoWorkEventHandler(loadUsersBackgroundAsync);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(loadUsersBackgroundAsyncCompleted);
            string[] parameters = { DirectoryControl.pathToPublicDirectoryKeys, DirectoryControl.pathToPrivateDirectoryKeys };
            this.Enabled = false;
            int fCountPublic = Directory.GetFiles(parameters[0], "*", SearchOption.TopDirectoryOnly).Length;
            int fCountPrivate = Directory.GetFiles(parameters[1], "*", SearchOption.TopDirectoryOnly).Length;
            this.progressBar1.Maximum = fCountPrivate + fCountPublic;
            backgroundWorker.RunWorkerAsync(parameters);
        }

        private void loadUsersBackgroundAsyncCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(() =>
            {
                this.Enabled = true;
            }));
            this.progressBar1.BeginInvoke(new MethodInvoker(() =>
            {

                this.progressBar1.Minimum = 0;
                this.progressBar1.Value = this.progressBar1.Minimum;
                this.progressBar1.Maximum = 100;
            }));



        }

        private void loadUsersBackgroundAsync(object sender, DoWorkEventArgs e)
        {
            string[] parameters = (string[])e.Argument;
            string[] namesOfFiles;
            namesOfFiles = Directory.GetFiles(parameters[0], "*", SearchOption.TopDirectoryOnly);
            foreach (string file in namesOfFiles)
            {
                PublicUser publicUser = PublicUser.DeserializePublicUser(Path.GetFileName(file));
                if (publicUser != null)
                {
                    publicUsers.Add(publicUser);
                }
                this.progressBar1.BeginInvoke(new MethodInvoker(() =>
                {
                    this.progressBar1.Value = this.progressBar1.Value + 1;
                }));
            }
            namesOfFiles = Directory.GetFiles(parameters[1], "*", SearchOption.TopDirectoryOnly);
            foreach (string file in namesOfFiles)
            {
                PrivateUser privateUser = PrivateUser.DeserializePrivateUser(Path.GetFileName(file));

                if (privateUser != null)
                {
                    privateUsers.Add(privateUser);
                }

                this.progressBar1.BeginInvoke(new MethodInvoker(() =>
                {
                    this.progressBar1.Value = this.progressBar1.Value + 1;
                }));
            }
            this.progressBar1.BeginInvoke(new MethodInvoker(() =>
            {
                this.progressBar1.Value = this.progressBar1.Maximum;
            }));



        }

        private void clearListBoxAsync(ListBox listBox)
        {
            listBox.BeginInvoke(new MethodInvoker(() =>
            {
                listBox.Items.Clear();
            }));
        }

        private void clearListBox(ref ListBox listBox)
        {
            listBox.Items.Clear();
        }

        

        public bool publicUserExist(string name)
        {
            if (publicUsers.Select(c => c.EmailOfUser).Contains(name) || privateUsers.Select(c => c.EmailOfUser).Contains(name))
            {
                return true;
            }
            else return false;
        }

        public bool privateUserExist(string name)
        {
            if (privateUsers.Select(c => c.EmailOfUser).Contains(name) || privateUsers.Select(c => c.EmailOfUser).Contains(name))
            {
                return true;
            }
            else return false;
        }

        private void progressChangedAsync(object sender, ProgressChangedEventArgs e)
        {

            if (e.ProgressPercentage != -1)
            {
                if (e.UserState != null)
                {
                    messageBox.postMessage((string)e.UserState);
                }
                this.progressBar1.Value = e.ProgressPercentage;
            }
            else
            {
                if (e.UserState != null)
                {
                    messageBox.postMessageAlert((string)e.UserState);
                }
            }

        }

        //nothing inside START
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ("Szyfrator" == tabControl1.SelectedTab.Text)
            {
                setTextBoxAsync(this.decryptPasswordTextBox, "");
                this.availableBoxDecrypt.Items.Clear();

            }
            else if ("Deszyfrator" == tabControl1.SelectedTab.Text)
            {
           /*     if (this.decryptSourceFileBox.Text != "")
                {
                    loadDecryptUsers(this.decryptSourceFileBox.Text);
                }*/
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void Dostępni_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        //nothing inside END


        //create user button click
        private void cryptCreateNewUserButtonClick(object sender, EventArgs e)
        {
            this.Enabled = false;
            //publicUsers.Cast<User>().ToList()
            using (var dialog = new CreateUserForm(this))
            {
                DialogResult createNewUserResult = dialog.ShowDialog();
                if (createNewUserResult == DialogResult.Cancel)
                {
                    messageBox.postMessage("Nie utworzono nowego użytkownika - zamknięto okno przez użytkownika");
                }
                else if (createNewUserResult == DialogResult.Abort)
                {

                }
                else if (createNewUserResult == DialogResult.OK)
                {
                    string newUser = dialog.returnValueOfForm;
                    loadNewUser(newUser + ".xml");
                }
            }
            this.Enabled = true;
        }

        //after create of user
        private void loadNewUser(string filename)
        {
            PublicUser publicUser = PublicUser.DeserializePublicUser(filename);
            PrivateUser privateUser = PrivateUser.DeserializePrivateUser(filename);
            publicUsers.Add(publicUser);
            privateUsers.Add(privateUser);
        }


        


        private static void setButtonEnabledAsync(Button button,bool set)
        {
            button.BeginInvoke(new MethodInvoker(() =>
            {
                button.Enabled = set;
            }));
        }

        private static void setButtonTextAsync(Button button, string text)
        {
            button.BeginInvoke(new MethodInvoker(() =>
            {
                button.Text = text;
            }));
        }

        private static void setProgessBarAsync(ProgressBar progressBar,int value)
        {
            progressBar.BeginInvoke(new MethodInvoker(() =>
            {
                progressBar.Value = value;
            }));
        }
        
        private static void setTextBoxAsync(TextBox textBox, string value)
        {
            textBox.BeginInvoke(new MethodInvoker(() =>
            {
                textBox.Text = value;
            }));
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void decryptLoadUsersButton_Click(object sender, EventArgs e)
        {
            loadDecryptUsers(this.decryptSourceFileBox.Text);
        }
    }
}
