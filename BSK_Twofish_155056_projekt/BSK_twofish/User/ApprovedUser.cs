﻿using System;
using System.Xml.Serialization;

namespace BSK_twofish
{
    [XmlType("User")]
    public class ApprovedUser
    {
        public ApprovedUser()
        {

        }

       

        private string email;
        [XmlElement("Email")]
        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }


        [XmlIgnore]
        public byte[] RSAkey { get; set; }



        private byte[] sessionKey;
        [XmlIgnore]
        public byte[] SessionKey
        {
            get
            {
                return sessionKey;
            }

            set
            {
                sessionKey = value;
                sessionKeyBase64Encoded =Convert.ToBase64String(value);
                
            }
        }

        private string sessionKeyBase64Encoded;
        [XmlElement("SessionKey")]
        public string SessionKeyBase64Encoded
        {
            get
            {
                return sessionKeyBase64Encoded;
            }

            set
            {
                sessionKeyBase64Encoded = value;
                sessionKey= Convert.FromBase64String(value);
            }
        }

        private string password;
        [XmlIgnore]
        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }


        





    }
}