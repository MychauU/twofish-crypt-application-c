﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
namespace BSK_twofish
{
    [XmlType( TypeName = "PublicUser")]
    public class PublicUser : User
    {
        public PublicUser()
        {

        }
        public PublicUser(string email, byte[] key)
        {
            this.EmailOfUser = email;
            this.PublicKeyOfUser = key;
            this.FileName = email + ".xml";
        }



        private string publicKeyOfUserString;
        [XmlIgnore]
        public string PublicKeyOfUserString
        {
            get
            {

                return publicKeyOfUserString;
            }

            set
            {
                publicKeyOfUserString = value;
                XmlDocument doc = new XmlDocument();
                doc.InnerXml = "<root>" + value + "</root>";
                this.publicKeyOfUserXmlNode = doc.DocumentElement.FirstChild;
                this.publicKeyOfUser = Cryptograph.getBytes(value);
            }
        }

        private XmlNode publicKeyOfUserXmlNode;
        [XmlElement("PublicRSAOfUser")]
        public XmlNode PublicKeyOfUserXmlNode
        {
            get
            {

                return publicKeyOfUserXmlNode;
            }

            set
            {
                publicKeyOfUserXmlNode = value;
                this.publicKeyOfUserString = value.OuterXml;
                this.publicKeyOfUser = Cryptograph.getBytes(value.OuterXml);

            }
        }


        private byte[] publicKeyOfUser;
        [XmlIgnore]
        public byte[] PublicKeyOfUser
        {
            get
            {
                return publicKeyOfUser;
            }

            set
            {
                publicKeyOfUser = value;
                publicKeyOfUserString= Cryptograph.getUTF8String(value);
                XmlDocument doc = new XmlDocument();
                doc.InnerXml = "<root>" + Cryptograph.getUTF8String(value) + "</root>";
                this.publicKeyOfUserXmlNode = doc.DocumentElement.FirstChild;
            }
        }

       

        public static bool serializePublicUser(PublicUser user)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(PublicUser));
                TextWriter textWriter = new StreamWriter(DirectoryControl.pathToPublicDirectoryKeys + user.FileName);
                serializer.Serialize(textWriter, user);
                textWriter.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static PublicUser DeserializePublicUser(String filenameWithExtension)
        {
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(PublicUser));
                TextReader textReader = new StreamReader(DirectoryControl.pathToPublicDirectoryKeys + filenameWithExtension);
                PublicUser ret = (PublicUser)deserializer.Deserialize(textReader);
                ret.FileName = filenameWithExtension;
                textReader.Close();
                return ret;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }

}
