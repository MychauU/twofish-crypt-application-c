﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace BSK_twofish
{
    [XmlType( TypeName = "PrivateUser")]
    public class PrivateUser : User
    {
        public PrivateUser()
        {

        }
        public PrivateUser(string email, byte[] key)
        {
            this.EmailOfUser = email;
            this.PrivateKeyOfUserEncoded = key;
            this.FileName = email + ".xml";
        }



        private byte[] privateKeyOfUserEncoded;
        [XmlIgnore]
        public byte[] PrivateKeyOfUserEncoded
        {
            get
            {
                return privateKeyOfUserEncoded;
            }
            set
            {
                privateKeyOfUserEncoded = value;
                this.privateKeyOfUserEncodedBase64 = Convert.ToBase64String(value);
            }

        }

        private string privateKeyOfUserEncodedBase64;
        [XmlElement(ElementName = "PrivateRSAOfUser")]
        public string PrivateKeyOfUserEncodedBase64
        {
            get
            {
                return privateKeyOfUserEncodedBase64;
            }
            set
            {
                privateKeyOfUserEncodedBase64 = value;
                privateKeyOfUserEncoded=Convert.FromBase64String(value);
            }

        }


        public static bool serializePrivateUser(PrivateUser user)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(PrivateUser));
                TextWriter textWriter = new StreamWriter(DirectoryControl.pathToPrivateDirectoryKeys + user.FileName);
                serializer.Serialize(textWriter, user);
                textWriter.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static PrivateUser DeserializePrivateUser(String filenameWithExtension)
        {
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(PrivateUser));
                TextReader textReader = new StreamReader(DirectoryControl.pathToPrivateDirectoryKeys + filenameWithExtension);
                PrivateUser ret = (PrivateUser)deserializer.Deserialize(textReader);
                ret.FileName = filenameWithExtension;
                textReader.Close();
                return ret;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}