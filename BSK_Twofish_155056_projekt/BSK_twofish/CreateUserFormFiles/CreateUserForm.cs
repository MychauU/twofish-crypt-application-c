﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BSK_twofish
{
    public partial class CreateUserForm : System.Windows.Forms.Form
    {

        BackgroundWorker backgroundWorker;
        /*  private const string MatchEmailPattern =
              @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
       + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                  [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
       + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                  [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
       + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
       */
        private const string MatchEmailPattern = @"^[^@\s]+@[^@\s]+\.[^@\s]+$";
        private Form1 parentForm;
        public string returnValueOfForm { get; set; }

        public CreateUserForm(Form1 parentForm)
        {
            this.parentForm = parentForm;
            InitializeComponent();
            initializeFormControls();
        }


        private void initializeFormControls()
        {
            this.progressBar1.Minimum = 0;
            this.progressBar1.Maximum = 100;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.button1.Text == "Anuluj")
            {
                this.button1.Enabled = false;
                backgroundWorker.CancelAsync();
            }
            else
            {
                string caption = @"Błąd tworzenia użytkownika";
                bool valid = true;
                StringBuilder errorMessage = new StringBuilder();
                String passwordText = this.passwordBox.Text;
                String rePasswordText = this.rePasswordBox.Text;
                String emailText = this.emailBox.Text;
                if (String.IsNullOrWhiteSpace(passwordText) || String.IsNullOrWhiteSpace(rePasswordText) || String.IsNullOrWhiteSpace(emailText))
                {
                    valid = false;
                    errorMessage.AppendLine(@"Tworzenie użytkownika  - istnieją puste pola");
                    MessageBox.Show(errorMessage.ToString(), caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (validateEmail(emailText) == false)
                    {
                        valid = false;
                        errorMessage.AppendLine(@"Tworzenie użytkownika  - zły format adresu email");
                    }
                    if (parentForm.publicUserExist(emailText) == true)
                    {
                        valid = false;
                        errorMessage.AppendLine(@"Tworzenie użytkownika  - email już istnieje - prosze podać inny email");
                    }
                    if (isPasswordsTheSame(passwordText, rePasswordText) == false)
                    {
                        valid = false;
                        errorMessage.AppendLine(@"Tworzenie użytkownika  - pole wpisz hasło ponownie nie zgadza się z polem hasło");
                    }
                    if (valid == false)
                    {
                        MessageBox.Show(errorMessage.ToString(), caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        backgroundWorker = new BackgroundWorker();
                        backgroundWorker.WorkerReportsProgress = true;
                        backgroundWorker.WorkerSupportsCancellation = true;
                        backgroundWorker.DoWork += new DoWorkEventHandler(BackgroundCreateNewUserThread);
                        Parameters parametersForWork = new Parameters(emailText, passwordText);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundCreateNewUserCompleted);
                        backgroundWorker.ProgressChanged += progressChangedAsync;
                        backgroundWorker.RunWorkerAsync(parametersForWork);
                        this.button1.Text = "Anuluj";
                    }
                }

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (backgroundWorker != null)
            {
                this.Enabled = false;
                backgroundWorker.CancelAsync();
            }


        }

        private void progressChangedAsync(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                this.label4.BeginInvoke(new MethodInvoker(() =>
                {
                    label4.Text = (string)e.UserState;
                }));
            }
            this.progressBar1.BeginInvoke(new MethodInvoker(() =>
            {
                progressBar1.Value = e.ProgressPercentage;
            }));
        }

        private void BackgroundCreateNewUserCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                resetFormComponentAsync();
            }
            else
            {
                returnValueOfForm = (string)e.Result;
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                ));
            }

        }


        //on another thread
        private void BackgroundCreateNewUserThread(object sender, DoWorkEventArgs e)
        {
            Parameters p = (Parameters)e.Argument;
            var result = createNewUserAsync(backgroundWorker, p);
            if (result == 1)
            {
                MessageBox.Show(@"Stworzono użytkownika o emailu " + p.Email + " - klucze zostały stworzone", "Stworzono użytkownika", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = false;
                e.Result = p.Email;

            }
            else if (result == 0)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = true;
                MessageBox.Show(@"Nie stworzono użytkownika o emailu " + p.Email + " - brak dostępu do tworzenia nowych plików - uruchom program jako administrator", "Nie stworzono użytkownika", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        private int createNewUserAsync(BackgroundWorker backgroundWorker, Parameters p)
        {
            //[0] public , [1] private
            backgroundWorker.ReportProgress(10, "Tworzenie kluczy RSA");
            string[] keyPairRSA = Cryptograph.createPairOfKeysRSA();
            if (backgroundWorker.CancellationPending)
            {
                return 0;
            }
            backgroundWorker.ReportProgress(60, "Szyfrowanie klucza prywatnego");
            byte[] keyPrivateEncoded = Cryptograph.encodeKeyPkcs7Padding(Cryptograph.getBytes(keyPairRSA[1]), Cryptograph.getBytes(p.Password));
            byte[] keyPublic = Cryptograph.getBytes(keyPairRSA[0]);
            if (backgroundWorker.CancellationPending)
            {
                return 0;
            }
            backgroundWorker.ReportProgress(80, "Tworzenie nowych plików uwierzytelniających");
            PublicUser publicUser = new PublicUser(p.Email, keyPublic);
            PrivateUser privateUser = new PrivateUser(p.Email, keyPrivateEncoded);
            if (PublicUser.serializePublicUser(publicUser) == false)
            {
                return 2;
            }
            if (PrivateUser.serializePrivateUser(privateUser) == false)
            {
                return 2;
            }
            backgroundWorker.ReportProgress(100, "Zakończono");
            return 1;

        }


        private void resetFormComponentAsync()
        {
            if (this.Enabled)
            {


                this.button1.BeginInvoke(new MethodInvoker(() =>
                {
                    button1.Text = "Stwórz";
                    button1.Enabled = true;
                }));
                this.passwordBox.BeginInvoke(new MethodInvoker(() =>
                {
                    this.passwordBox.Text = "";
                }));
                this.rePasswordBox.BeginInvoke(new MethodInvoker(() =>
                {
                    this.rePasswordBox.Text = "";
                }));
                this.label4.BeginInvoke(new MethodInvoker(() =>
                {
                    this.label4.Text = "";
                }));
                this.progressBar1.BeginInvoke(new MethodInvoker(() =>
                {
                    this.progressBar1.Value = 0;
                }));
            }
        }


        private bool isPasswordsTheSame(string passwordText, string rePasswordText)
        {
            if (String.Equals(passwordText, rePasswordText))
                return true;
            else return false;
        }


        private bool validateEmail(string emailText)
        {

            if (String.IsNullOrWhiteSpace(emailText))
            {
                return false;
            }
            else if (Regex.IsMatch(emailText, MatchEmailPattern))
                return true;
            else return false;
        }



        private class Parameters
        {
            public Parameters(string email, string password)
            {
                this.email = email;
                this.password = password;
            }
            string email;
            string password;
            public string Email
            {
                get
                {
                    return email;
                }
            }

            public string Password
            {
                get
                {
                    return password;
                }


            }
        }

        private void CreateUserForm_Load(object sender, EventArgs e)
        {

        }
    }
}
