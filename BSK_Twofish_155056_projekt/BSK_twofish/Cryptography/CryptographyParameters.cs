﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace BSK_twofish
{
    [XmlType(TypeName = "EncryptedFileHeader")]
    public class CryptographyParameters
    {

        [XmlIgnore]
        public bool forEncryption;

        [XmlIgnore]
        public string sourceFilePathWithFilename { get; set; }
        [XmlIgnore]
        public string destinationFilePathWithFilename { get; set; }

        [XmlIgnore]
        public BackgroundWorker bw { get; set; }

        [XmlIgnore]
        public ApprovedUser decryptTargetUser { get; set; }

        [XmlIgnore]
        public long sizeOfHeader { get; set; }

        private string algorithmName;
        [XmlElement("Algorithm")]
        public string AlgorithmName
        {
            get
            {
                return algorithmName;
            }

            set
            {
                algorithmName = value;
            }
        }

        private string keyLength;
        [XmlElement("KeySize")]
        public string KeyLength
        {
            get
            {
                return keyLength;
            }

            set
            {
                keyLength = value;
            }
        }

        private string segmentSize;
        [XmlElement("SegmentSize")]
        public string SegmentSize
        {
            get
            {
                return segmentSize;
            }

            set
            {
                segmentSize = value;
            }
        }


        private string mode;
        [XmlElement("CipherMode")]
        public string Mode
        {
            get
            {
                return mode;
            }

            set
            {
                mode = value;
            }
        }
        private byte[] iv;
        [XmlIgnore]
        public byte[] Iv
        {
            get
            {
                return iv;
            }

            set
            {
                iv = value;
                ivBase64Encoded = Convert.ToBase64String(value);
            }
        }

        private string ivBase64Encoded;
        [XmlElement("IV")]
        public string IvBase64Encoded
        {
            get
            {
                return ivBase64Encoded;
            }

            set
            {
                ivBase64Encoded = value;
                iv = Convert.FromBase64String(value);
            }
        }


        private List<ApprovedUser> approvedUsers;
        [XmlArray("ApprovedUsers")]
        public List<ApprovedUser> ApprovedUsers
        {
            get
            {
                return approvedUsers;
            }

            set
            {
                approvedUsers = value;
            }
        }



        public CryptographyParameters()
        {

        }

        public CryptographyParameters(BackgroundWorker bw)
        {
            this.bw = bw;
            ApprovedUsers = new List<ApprovedUser>();

        }


        public void setApprovedUser(string name, byte[] key)
        {
            ApprovedUser approvedUser = new ApprovedUser();
            approvedUser.RSAkey = key;
            approvedUser.Email = name;
            ApprovedUsers.Add(approvedUser);
        }

        public byte[] getSessionKey(string chosenUser)
        {
            var returnValue = this.approvedUsers.FirstOrDefault((x) => x.Email == chosenUser);
            return returnValue.SessionKey;
        }
    }
}
