﻿using System;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Modes;
using System.Xml.Serialization;

namespace BSK_twofish
{
    public static class Cryptograph
    {
        private static SecureRandom random = new SecureRandom();
        public static byte[] getBytes(string str)
        {

            byte[] bytes = Encoding.UTF8.GetBytes(str);
            return bytes;
        }

        public static string getUTF8String(byte[] bytes)
        {
            string str = Encoding.UTF8.GetString(bytes);
            return str;
        }

        public static string[] createPairOfKeysRSA()
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(4096);
            RSAParameters RSAKeyInfo = RSA.ExportParameters(true);
            string publicKey = RSA.ToXmlString(false);
            string privateKey = RSA.ToXmlString(true);
            string[] pom = { publicKey, privateKey };
            return pom;
        }

        public static void encryptFile(object sender, DoWorkEventArgs e)
        {
            CryptographyParameters cryptographyParameters = (CryptographyParameters)e.Argument;
            Org.BouncyCastle.Crypto.Engines.TwofishEngine twofish = new Org.BouncyCastle.Crypto.Engines.TwofishEngine();
            BufferedBlockCipher blockCipher = blockCipher = new PaddedBufferedBlockCipher(twofish, new Pkcs7Padding()); //ecb
            byte[] sessionKey = null;
            byte[] iv = new byte[128/8];
            random.NextBytes(iv);
            var keyParam = new KeyGenerationParameters(random, Int32.Parse(cryptographyParameters.KeyLength));
            var keyGenerator = new CipherKeyGenerator();
            keyGenerator.Init(keyParam);
            sessionKey = keyGenerator.GenerateKey();
            KeyParameter keyParameter = new KeyParameter(sessionKey);
            if (cryptographyParameters.Mode == "ECB")
            {
                blockCipher.Init(cryptographyParameters.forEncryption, keyParameter);
            }
            else
            {
                if (cryptographyParameters.Mode == "CBC")
                {
                    blockCipher = new PaddedBufferedBlockCipher(new CbcBlockCipher(twofish), new Pkcs7Padding());
                }
                else
                {
                    if (cryptographyParameters.Mode == "CFB")
                    {
                        blockCipher = new BufferedBlockCipher(new CfbBlockCipher(twofish, Int32.Parse(cryptographyParameters.SegmentSize))); //cfb
                    }
                    else if (cryptographyParameters.Mode == "OFB")
                    {
                        blockCipher = new BufferedBlockCipher(new OfbBlockCipher(twofish, Int32.Parse(cryptographyParameters.SegmentSize))); //ofb
                    }
                    else
                    {
                        cryptographyParameters.bw.ReportProgress(-1, "Błąd - Nieznany tryb szyfrowania blokowego.");
                        e.Cancel = true;
                        return;
                    }
                }
                cryptographyParameters.Iv = iv;
                ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParameter, iv, 0, 16);
                blockCipher.Init(cryptographyParameters.forEncryption, keyParamWithIV);
            }
            if (encodeSessionKeyApprovedUsers(cryptographyParameters, sessionKey) == false)
            {
                cryptographyParameters.bw.ReportProgress(-1, "Błąd szyfrowania - jeden z kluczy publicznych nie nadaje się do użytku");
                e.Cancel = true;
                return;
            }
            BinaryReader br = null;
            BinaryWriter bw = null;
            try { br = new BinaryReader(File.OpenRead(cryptographyParameters.sourceFilePathWithFilename)); }
            catch
            {
                cryptographyParameters.bw.ReportProgress(-1, "Błąd otwierania pliku źródłowego.");
                e.Cancel = true;
                return;
            }
            try
            {
                bw = new BinaryWriter(File.OpenWrite(cryptographyParameters.destinationFilePathWithFilename));
            }
            catch
            {
                cryptographyParameters.bw.ReportProgress(-1, "Błąd otwierania pliku wynikowego.");
                e.Cancel = true;
                return;
            }
            try
            {
                SerializeHeaderToEncryptFile(cryptographyParameters, bw);
            }
            catch
            {
                cryptographyParameters.bw.ReportProgress(-1, "Błąd zapisu nagłówka kryptowanego pliku.");
                e.Cancel = true;
                return;
            }
            int blockSize = blockCipher.GetBlockSize();
            long startPosition = br.BaseStream.Position;
            long endValue = br.BaseStream.Length - startPosition;
            int i = 0;
            byte[] buffer= new byte[blockSize];
            byte[] encBuffer = new byte[512];
            int lastValue = 0;
            while (br.BaseStream.Position < br.BaseStream.Length)
            {
                if (cryptographyParameters.bw.CancellationPending)
                {
                    bw.Flush();
                    br.Close();
                    bw.Close();
                    cryptographyParameters.bw.ReportProgress(-1, "Przerwano przez użytkownika.");
                    e.Cancel = true;
                    try
                    {
                        File.Delete(cryptographyParameters.destinationFilePathWithFilename);
                    }
                    catch
                    {
                        cryptographyParameters.bw.ReportProgress(-1, "Nie udało się skasować pliku celu (częsciowo zaszyfrowanego), proszę usunąć ręcznie.");
                    }
                    return;
                }
                i++;
                if (i > 100)
                {
                    cryptographyParameters.bw.ReportProgress((int)((br.BaseStream.Position - startPosition) * 100 / endValue));
                    i = 0;
                }
                if (br.BaseStream.Length - br.BaseStream.Position > blockSize)
                {
                    buffer = br.ReadBytes(blockSize);
                    lastValue = blockCipher.ProcessBytes(buffer, 0, buffer.Length, encBuffer, 0);
                    bw.Write(encBuffer, 0, lastValue);
                }
                else
                {
                    buffer = br.ReadBytes(blockSize);
                }
                
            }
            lastValue = blockCipher.DoFinal(buffer, 0, buffer.Length, encBuffer, 0);
            bw.Write(encBuffer, 0, lastValue);
            cryptographyParameters.bw.ReportProgress(100);
            bw.Flush();
            br.Close();
            bw.Close();
            if (cryptographyParameters.forEncryption)
                e.Result = "Zaszyfrowano plik pod ścieżką " + cryptographyParameters.destinationFilePathWithFilename;
            else
                e.Result = "Odszyfrowano plik pod ścieżką " + cryptographyParameters.destinationFilePathWithFilename;
        }


        public static void decryptFile(object sender, DoWorkEventArgs e)
        {
            CryptographyParameters cryptographyParameters = (CryptographyParameters)e.Argument;
            Org.BouncyCastle.Crypto.Engines.TwofishEngine twofish = new Org.BouncyCastle.Crypto.Engines.TwofishEngine();
            BufferedBlockCipher blockCipher = null;
            bool failPrivateKey = false;
            byte[] privatekey = decodeKeyPkcs7Padding(
                    cryptographyParameters.decryptTargetUser.RSAkey, getBytes(cryptographyParameters.decryptTargetUser.Password)
                    );
            byte[] sessionKey = new byte[Int32.Parse(cryptographyParameters.KeyLength)/8];
            if (privatekey == null)
            {
                //here means that password was not correc thus
                //private key could not be decrypted thus
                //sessionKey could not be decrypted thus
                //padding can't be correctly pull out
                //but we make some random sessionKey to discourage attack 
                failPrivateKey = true;
                byte[] hashValue256 = null;
                using (SHA256 sha256 = new SHA256Managed())
                {
                    hashValue256 = sha256.ComputeHash(getBytes(cryptographyParameters.decryptTargetUser.Email+cryptographyParameters.decryptTargetUser.Password));
                    Array.Copy(hashValue256,0, sessionKey, 0, sessionKey.Length);

                }
                
            }
            else
            {
                sessionKey = decodeSessionKeyFromTargetApprovedUser(cryptographyParameters, privatekey);
            }  
            if (sessionKey == null)
            {
                //here means that password was not correct thus
                //private key could  be decrypted thus
                //but private key was tainted/incorrect thus 
                //sessionKey could not be decrypted thus
                //padding can't be correctly pull out
                //but we make some random sessionKey to discourage attack 
                failPrivateKey = true;
                byte[] hashValue256 = null;
                using (SHA256 sha256 = new SHA256Managed())
                {
                    hashValue256 = sha256.ComputeHash(getBytes(cryptographyParameters.decryptTargetUser.Email+cryptographyParameters.decryptTargetUser.Password));
                    Array.Copy(hashValue256, 0, sessionKey, 0, sessionKey.Length);

                }
            }
            KeyParameter keyParameter = new KeyParameter(sessionKey);
            if (cryptographyParameters.Mode == "ECB")
            {
                blockCipher = new PaddedBufferedBlockCipher(twofish, new Pkcs7Padding()); //ecb
                blockCipher.Init(cryptographyParameters.forEncryption, keyParameter);
            }
            else
            {
                if (cryptographyParameters.Mode == "CBC")
                {
                    blockCipher = new PaddedBufferedBlockCipher(new CbcBlockCipher(twofish), new Pkcs7Padding()); //cbc
                }
                else
                {
                    if (cryptographyParameters.Mode == "CFB")
                    {
                        blockCipher = new BufferedBlockCipher(new CfbBlockCipher(twofish, Int32.Parse(cryptographyParameters.SegmentSize))); //cfb
                    }
                    else if (cryptographyParameters.Mode == "OFB")
                    {

                        blockCipher = new BufferedBlockCipher(new OfbBlockCipher(twofish, Int32.Parse(cryptographyParameters.SegmentSize))); //ofb
                    }
                    else
                    {
                        cryptographyParameters.bw.ReportProgress(-1, "Błąd - Nieznany tryb szyfrowania blokowego.");
                        e.Cancel = true;
                        return;
                    }
                }
                ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParameter, cryptographyParameters.Iv, 0, 16);
                blockCipher.Init(cryptographyParameters.forEncryption, keyParamWithIV);
            }
            BinaryReader br = null;
            BinaryWriter bw = null;
            try { br = new BinaryReader(File.OpenRead(cryptographyParameters.sourceFilePathWithFilename)); }
            catch
            {
                cryptographyParameters.bw.ReportProgress(-1, "Błąd otwierania pliku źródłowego.");
                e.Cancel = true;
                return;
            }
            try { bw = new BinaryWriter(File.OpenWrite(cryptographyParameters.destinationFilePathWithFilename)); }
            catch
            {
                cryptographyParameters.bw.ReportProgress(-1, "Błąd otwierania pliku wynikowego.");
                e.Cancel = true;
                return;
            }
            byte[] headerBytes = br.ReadBytes((int)cryptographyParameters.sizeOfHeader);
            int blockSize = blockCipher.GetBlockSize();
            long startPosition = br.BaseStream.Position;
            long endValue = br.BaseStream.Length - startPosition;
            int i = 0;
            long wtf = br.BaseStream.Position;
            byte[] buffer=new byte[blockSize];
            byte[] encBuffer = new byte[512];
            int lastValue = 0;
            while (br.BaseStream.Position < br.BaseStream.Length)
            {
                if (cryptographyParameters.bw.CancellationPending)
                {
                    bw.Flush();
                    br.Close();
                    bw.Close();
                    cryptographyParameters.bw.ReportProgress(-1, "Przerwano przez użytkownika.");
                    e.Cancel = true;
                    try
                    {
                        File.Delete(cryptographyParameters.destinationFilePathWithFilename);
                    }
                    catch
                    {
                        cryptographyParameters.bw.ReportProgress(-1, "Nie udało się skasować pliku celu (częsciowo odszyfrowanego), proszę usunąć ręcznie.");
                    }

                    return;
                }

                i++;
                if (i > 100)
                {
                    cryptographyParameters.bw.ReportProgress((int)((br.BaseStream.Position - startPosition) * 100 / endValue));
                    i = 0;
                }


                if (br.BaseStream.Length - br.BaseStream.Position > blockSize)
                {
                    buffer = br.ReadBytes(blockSize);
                    lastValue = blockCipher.ProcessBytes(buffer, 0, buffer.Length, encBuffer, 0);
                    bw.Write(encBuffer, 0, lastValue);
                }
                else
                {
                    buffer = br.ReadBytes(blockSize);
               //     Array.Copy(buffer, 0, encBuffer, 0, buffer.Length);
                }
                

            }
            if (failPrivateKey)
            {
                lastValue = blockCipher.ProcessBytes(buffer, 0, buffer.Length, encBuffer, 0);
                bw.Write(encBuffer, 0, lastValue);
                //here means that password was (not) correct thus
                //private key could (not) be decrypted thus
                //sessionKey could not be decrypted thus
                //padding can't be correctly pull out
            }
            else
            {
                lastValue = blockCipher.DoFinal(buffer,0,buffer.Length,encBuffer, 0);
                bw.Write(encBuffer, 0, lastValue);
            }
            

            cryptographyParameters.bw.ReportProgress(100);

            bw.Flush();
            br.Close();
            bw.Close();
            if (cryptographyParameters.forEncryption)
                e.Result = "Zaszyfrowano plik pod ścieżką " + cryptographyParameters.destinationFilePathWithFilename;
            else
                e.Result = "Odszyfrowano plik pod ścieżką " + cryptographyParameters.destinationFilePathWithFilename;
        }


        //sha256  , encoded  padding  pkcs7, 
        public static byte[] encodeKeyPkcs7Padding(byte[] key, byte[] password)
        {
            byte[] hashValue256;
            using (SHA256 sha256 = new SHA256Managed())
            {
                hashValue256 = sha256.ComputeHash(password);
            }
            Org.BouncyCastle.Crypto.Engines.TwofishEngine twofish = new Org.BouncyCastle.Crypto.Engines.TwofishEngine();
            KeyParameter keyparam = new KeyParameter(hashValue256);
            PaddedBufferedBlockCipher blockCipher = new PaddedBufferedBlockCipher(twofish, new Pkcs7Padding()); 
            blockCipher.Init(true, keyparam);
            try
            {
                byte[] outputBytes = new byte[blockCipher.GetOutputSize(key.Length)];
                int length = blockCipher.ProcessBytes(key, outputBytes, 0);
                blockCipher.DoFinal(outputBytes, length); 
                return outputBytes;
            }
            catch
            {
                return null;
            }

        }

        public static byte[] decodeKeyPkcs7Padding(byte[] key, byte[] password)
        {
            byte[] hashValue256;
            using (SHA256 sha256 = new SHA256Managed())
            {
                hashValue256 = sha256.ComputeHash(password);
            }
            Org.BouncyCastle.Crypto.Engines.TwofishEngine twofish = new Org.BouncyCastle.Crypto.Engines.TwofishEngine();
            KeyParameter keyparam = new KeyParameter(hashValue256);
            PaddedBufferedBlockCipher blockCipher = new PaddedBufferedBlockCipher(twofish, new Pkcs7Padding()); //ecb
            blockCipher.Init(false, keyparam);
            byte[] outputBytes = new byte[blockCipher.GetOutputSize(key.Length)];
            try
            {
                int length = blockCipher.ProcessBytes(key, outputBytes, 0);
                blockCipher.DoFinal(outputBytes, length); //Do the final block
                var i = outputBytes.Length - 1;
                while (outputBytes[i] == 0)
                {
                    --i;
                }
                var temp = new byte[i + 1];
                Array.Copy(outputBytes, temp, i + 1);
                return temp;
            }
            catch
            {
                return null;
            }
        }



        private static bool SerializeHeaderToEncryptFile(CryptographyParameters fileHeader, BinaryWriter writer)
        {

            XmlSerializer serializer = new XmlSerializer(typeof(CryptographyParameters));
            TextWriter textWriter = new StringWriter();
            serializer.Serialize(textWriter, fileHeader);
            string header = textWriter.ToString();
            textWriter.Close();
            byte[] array = getBytes(header);
            writer.Write(array);
            return true;


        }

        public static CryptographyParameters DeserializeHeaderToDecryptFile(BinaryReader binaryReader)
        {

            String regexValue = "</EncryptedFileHeader>";
            StringBuilder header = new StringBuilder();
            bool isBigFile = false;
            byte[] headerBytes = null;
            int indexOfEndXml = -1;
            headerBytes = binaryReader.ReadBytes(1024);
            header.Append(getUTF8String(headerBytes));
            String pom = header.ToString();
            indexOfEndXml = pom.IndexOf("<EncryptedFileHeader");
            if (indexOfEndXml  == -1)
            {
                throw new Exception();
            }
            if (binaryReader.BaseStream.Length > 1000000)
            {
                isBigFile = true;
                //means big file >1MB
            }
            
            do
            {
                headerBytes = binaryReader.ReadBytes(1024);
                header.Append(getUTF8String(headerBytes));
                pom = header.ToString();
                indexOfEndXml = pom.IndexOf(regexValue);
                if (isBigFile)
                {
                    if (indexOfEndXml == -1 && (binaryReader.BaseStream.Position < 1000000))
                    {

                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    if (indexOfEndXml == -1 && (binaryReader.BaseStream.Position < binaryReader.BaseStream.Length / 2))
                    {

                    }
                    else
                    {
                        break;
                    }
                }
            } while (true);

            if (indexOfEndXml > -1)
            {
                header.Remove(indexOfEndXml + regexValue.Length, header.Length - (indexOfEndXml + regexValue.Length));
                XmlSerializer serializer = new XmlSerializer(typeof(CryptographyParameters));
                TextReader textReader = new StringReader(header.ToString());
                CryptographyParameters cp = (CryptographyParameters)serializer.Deserialize(textReader);
                cp.sizeOfHeader = indexOfEndXml + regexValue.Length;
                return cp;
            }
            else
            {
                throw new Exception();
            }

        }



        private static bool encodeSessionKeyApprovedUsers(CryptographyParameters cryptographyParameters, byte[] sessionKey)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

            foreach (ApprovedUser au in cryptographyParameters.ApprovedUsers)
            {
                try
                {
                    RSA.FromXmlString(getUTF8String(au.RSAkey));
                    au.SessionKey = RSA.Encrypt(sessionKey, true);
                    au.SessionKeyBase64Encoded= Convert.ToBase64String(au.SessionKey);
                }
                catch
                {
                    return false;
                }
            }
            return true;


        }

        private static byte[] decodeSessionKeyFromTargetApprovedUser(CryptographyParameters cryptographyParameters, byte[] RSAKey)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            try
            {
                RSA.FromXmlString(getUTF8String(RSAKey));
                return RSA.Decrypt(cryptographyParameters.decryptTargetUser.SessionKey, true);
            }
            catch
            {
                return null;
            }


        }
    }


}


