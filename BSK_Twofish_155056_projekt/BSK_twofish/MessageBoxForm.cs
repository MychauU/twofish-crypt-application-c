﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BSK_twofish
{
    public class MessageBoxForm : RichTextBox
    {
        private long numberLineOfMessageBox;
        public MessageBoxForm() : base()
        {
            this.AllowDrop = false;
            this.ReadOnly = true;
            this.ScrollBars = RichTextBoxScrollBars.Vertical;
            numberLineOfMessageBox = 1;
        }

        public long NumberLineOfMessageBox
        {
            get
            {
                return numberLineOfMessageBox;
            }
        }
        public void postMessage(string message)
        {
            this.AppendText(numberLineOfMessageBox++ + " " + message + Environment.NewLine);
            this.SelectionStart = this.Text.Length;
            this.ScrollToCaret();
        }

        public void postMessageAsync(string message)
        {
            this.BeginInvoke(new MethodInvoker(() =>
            {
                this.AppendText(numberLineOfMessageBox++ + " " + message + Environment.NewLine);
                this.SelectionStart = this.Text.Length;
                this.ScrollToCaret();
            }));

        }

        public void postMessageAlert(string message)
        {
            this.SelectionStart = this.TextLength;
            this.SelectionLength = 0;

            this.SelectionColor = Color.Red;
            this.AppendText(numberLineOfMessageBox++ + " " + message + Environment.NewLine);
            this.SelectionStart = this.Text.Length;
            this.ScrollToCaret();
            this.SelectionColor = this.ForeColor;

        }

        public void postMessageAlertAsync(string message)
        {
            this.BeginInvoke(new MethodInvoker(() =>
            {
                this.SelectionStart = this.TextLength;
                this.SelectionLength = 0;
                this.SelectionColor = Color.Red;
                this.AppendText(numberLineOfMessageBox++ + " " + message + Environment.NewLine);
                this.SelectionStart = this.Text.Length;
                this.ScrollToCaret();
                this.SelectionColor = this.ForeColor;
            }));
        }
    }
}
