﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSK_twofish
{
    public static class DirectoryControl
    {
        public static string pathToPublicDirectoryKeys = @".\keys\public\";
        public static string pathToPrivateDirectoryKeys = @".\keys\private\";
        public static string pathToDirectoryKeys = @".\keys\";
        public static void createDirectory()
        {
            bool exists = false;
            if (!Directory.Exists(pathToDirectoryKeys))
            {
                exists = true;
                Directory.CreateDirectory(pathToDirectoryKeys);
                Directory.CreateDirectory(pathToPublicDirectoryKeys);
                Directory.CreateDirectory(pathToPrivateDirectoryKeys);
                

            }
            if (exists == false)
            {
                if (!Directory.Exists(pathToPublicDirectoryKeys))
                {
                    Directory.CreateDirectory(pathToPublicDirectoryKeys);
                }
                if (!Directory.Exists(pathToPrivateDirectoryKeys))
                {
                    Directory.CreateDirectory(pathToPrivateDirectoryKeys);
                }

            }
        }

    }
}
