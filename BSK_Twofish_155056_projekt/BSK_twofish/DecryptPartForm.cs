﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace BSK_twofish
{
    public partial class Form1
    {
        private void decryptSourceFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseFileDialog = new OpenFileDialog();
            chooseFileDialog.FilterIndex = 1;
            chooseFileDialog.Title = "Wybierz plik do odszyfrowania";
            chooseFileDialog.Multiselect = false;
            if (chooseFileDialog.ShowDialog() == DialogResult.OK)
            {
                messageBox.postMessage("Próba załadowania listy odbiorców z pliku");
                string filenameWithPath = chooseFileDialog.FileName;
                decryptSourceFileBox.Text = filenameWithPath;
                if (loadDecryptUsers(filenameWithPath))
                {
                    messageBox.postMessage("Poprawnie wybrano plik źródłowy do odszyfrowania");
                }
                

            }
        }
        private void decryptDestinationFileButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog chooseFileDialog = new SaveFileDialog();
            chooseFileDialog.Filter = "All Files (*.*)|*.*";
            chooseFileDialog.FilterIndex = 1;
            chooseFileDialog.Title = "Wybierz lokalizację pliku odszyfrowanego oraz wpisz nazwę";
            if (chooseFileDialog.ShowDialog() == DialogResult.OK)
            {
                decryptDestinationFileBox.Text = chooseFileDialog.FileName;
                messageBox.postMessage("Poprawnie wybrano miejsce zapisu pliku docelowego do odszyfrowania");
            }
        }

        private bool loadDecryptUsers(string pathWithFilename)
        {
            FileStream filestream = null;
            try
            {
                filestream = new FileStream(pathWithFilename, FileMode.Open);
            }
            catch 
            {
                messageBox.postMessageAlert("Nie można otworzyć pliku źródłowego w celu pobrania podstawowych danych");
                clearListBox(ref availableBoxDecrypt);
                return false;
            }
            BinaryReader br = new BinaryReader(filestream);
            CryptographyParameters cryptographyParametersForDecrypt = null;
            try
            {
                cryptographyParametersForDecrypt = Cryptograph.DeserializeHeaderToDecryptFile(br);
            }
            catch
            {
                messageBox.postMessageAlert("Zły nagłówek zaszyfrowanego pliku - nie można załadować danych do odszyfrowania");
                br.Close();
                clearListBox(ref availableBoxDecrypt);
                filestream.Close();
                return false;
            }
            clearListBox(ref availableBoxDecrypt);
            fillReceiverList(cryptographyParametersForDecrypt);
            br.Close();
            filestream.Close();
            return true;
        }

        private void decryptButton_Click(object sender, EventArgs e)
        {
            if (this.decryptButton.Text == "Deszyfruj")
            {
                if (areDecryptBoxesFilled())
                {
                    backgroundWorker = new BackgroundWorker();
                    FileStream filestream = null;
                    try
                    {
                        filestream = new FileStream(this.decryptSourceFileBox.Text, FileMode.Open);
                    }
                    catch
                    {
                        messageBox.postMessageAlert("Nie można otworzyć pliku źródłowego");
                        return;
                    }
                    BinaryReader br = new BinaryReader(filestream);
                    CryptographyParameters cryptographyParameters= null;
                    try
                    {
                        cryptographyParameters = Cryptograph.DeserializeHeaderToDecryptFile(br);
                    }
                    catch
                    {
                        messageBox.postMessageAlert("Zły nagłówek zaszyfrowanego pliku - nie można załadować danych do odszyfrowania");
                        br.Close();
                        filestream.Close();
                        return;
                    }
                    br.Close();
                    filestream.Close();
                    cryptographyParameters.bw = backgroundWorker;
                    cryptographyParameters.forEncryption = false;
                    cryptographyParameters.sourceFilePathWithFilename = this.decryptSourceFileBox.Text;
                    cryptographyParameters.destinationFilePathWithFilename = this.decryptDestinationFileBox.Text;
                    string chosenUser =(string) this.availableBoxDecrypt.SelectedItem;
                    
                    PrivateUser privateUserForDecryption = getPrivateUser(chosenUser);
                    if (privateUserForDecryption == null)
                    {
                        messageBox.postMessageAlert("Dla wybranego użytkownika program nie posiada klucza prywatnego");
                        return;
                    }
                    ApprovedUser approvedUser = new ApprovedUser();
                    approvedUser.Email = privateUserForDecryption.EmailOfUser;
                    approvedUser.RSAkey = privateUserForDecryption.PrivateKeyOfUserEncoded;
                    approvedUser.Password = this.decryptPasswordTextBox.Text;
                    approvedUser.SessionKey = cryptographyParameters.getSessionKey(chosenUser);
                    cryptographyParameters.decryptTargetUser = approvedUser;
                    backgroundWorker.WorkerReportsProgress = true;
                    backgroundWorker.WorkerSupportsCancellation = true;
                    backgroundWorker.DoWork += new DoWorkEventHandler(Cryptograph.decryptFile);
                    backgroundWorker.RunWorkerCompleted += decryptFileDoneAsync;
                    backgroundWorker.ProgressChanged += progressChangedAsync;
                    messageBox.postMessage("Deszyfrowanie...");
                    this.decryptButton.Text = "Anuluj";
                    this.cryptButton.Enabled = false;
                    backgroundWorker.RunWorkerAsync(cryptographyParameters);
                }
                else
                {
                    messageBox.postMessage("Proces deszyfrowania nie uruchomił się");
                }

            }
            else
            {
                this.decryptPasswordTextBox.Text = "";
                this.decryptButton.Enabled = false;
                messageBox.postMessage("Anulowanie...");
                backgroundWorker.CancelAsync();
            }
        }

        private void decryptFileDoneAsync(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                messageBox.postMessageAsync("Deszyfrowanie przerwane.");
            }
            else
            {
                string result = (string)e.Result;
                messageBox.postMessageAsync("Deszyfrowanie wykonane. " + result);
            }
            setButtonEnabledAsync(this.decryptButton, true);
            setButtonTextAsync(this.decryptButton, "Deszyfruj");
            setButtonEnabledAsync(this.cryptButton, true);
            setProgessBarAsync(this.progressBar1, 0);
            setTextBoxAsync(this.decryptPasswordTextBox, "");
           // setTextBoxAsync(this.decryptDestinationFileBox, "");
            //setTextBoxAsync(this.decryptSourceFileBox, "");
            clearListBoxAsync(this.availableBoxDecrypt);

        }

        private bool areDecryptBoxesFilled()
        {
            bool isGood = true;
            StringBuilder str = new StringBuilder();
            if (String.IsNullOrWhiteSpace(this.decryptSourceFileBox.Text))
            {
                isGood = false;
                str.Append("Nie wybrałeś pliku do deszyfrowania. ");
            }
            else
            {
                if (!File.Exists(this.decryptSourceFileBox.Text))
                {
                    isGood = false;
                    str.Append("Plik źródłowy nie istnieje. ");
                }
            }
            if (String.IsNullOrWhiteSpace(this.decryptDestinationFileBox.Text))
            {
                isGood = false;
                str.Append("Nie wybrałeś miejsca i nazwę docelowego, odszyfrowanego pliku. ");
            }
            if (this.decryptSourceFileBox.Text == this.decryptDestinationFileBox.Text)
            {
                isGood = false;
                str.Append("Plik źródłowy i docelowy jest taki sam! Podaj inną nazwę pliku docelowego. ");
            }
            if ((string)this.availableBoxDecrypt.SelectedItem==null)
            {
                isGood = false;
                str.Append("Nie wybrałeś użytkownika. ");
            }
            if (String.IsNullOrWhiteSpace(this.decryptPasswordTextBox.Text))
            {
                isGood = false;
                str.Append("Nie wpisałeś hasła. ");
            }
            if (isGood == false)
            {
                this.messageBox.postMessageAlert(str.ToString());
                return false;
            }
            else return true;
        }


        private void fillReceiverList(CryptographyParameters cryptographyParameters)
        {
            foreach (ApprovedUser approvedUser in cryptographyParameters.ApprovedUsers)
            {
                if (privateUserExist(approvedUser.Email))
                {
                    availableBoxDecrypt.Items.Add(approvedUser.Email);
                }
                else
                {
                    availableBoxDecrypt.Items.Add(approvedUser.Email+" - brak klucza");
                }
            }
        }


        
    }
}
