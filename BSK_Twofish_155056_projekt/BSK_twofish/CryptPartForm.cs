﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace BSK_twofish
{
    public partial class Form1
    {
        private void SetCryptListBoxAsync(ListBox listBox, List<PublicUser> publicUsers)
        {
            listBox.BeginInvoke(new MethodInvoker(() =>
            {
                foreach (PublicUser user in publicUsers)
                {
                    listBox.Items.Add(user.EmailOfUser);
                }
            }));

        }

        private void SetCryptListBoxAsync(ListBox listBox, PublicUser publicUser)
        {
            listBox.BeginInvoke(new MethodInvoker(() =>
            {
                listBox.Items.Add(publicUser.EmailOfUser);
            }));

        }

        private void SetCryptListBox(ref ListBox listBox, List<PublicUser> publicUsers)
        {
            foreach (PublicUser user in publicUsers)
            {
                listBox.Items.Add(user.EmailOfUser);
            }
        }

        private void SetCryptListBox(ref ListBox listBox, PublicUser publicUser)
        {
            listBox.Items.Add(publicUser.EmailOfUser);
        }

        private void PublicUsers_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                PublicUser newItem = this.publicUsers.ElementAt(e.NewIndex);
                this.SetCryptListBoxAsync(this.availableBoxCrypt, newItem);
            }
        }

        private void cryptDestinationFileButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog chooseFileDialog = new SaveFileDialog();
            chooseFileDialog.Filter = "All Files (*.*)|*.*";
            chooseFileDialog.FilterIndex = 1;
            chooseFileDialog.Title = "Wybierz lokalizację pliku zaszyfrowanego oraz wpisz nazwę";
            if (chooseFileDialog.ShowDialog() == DialogResult.OK)
            {
                cryptDestinationFileBox.Text = chooseFileDialog.FileName;
                messageBox.postMessage("Poprawnie wybrano miejsce zapisu pliku docelowego do szyfrowania");
            }
        }

        private void cryptChooseSourceFileButtonClick(object sender, EventArgs e)
        {
            OpenFileDialog chooseFileDialog = new OpenFileDialog();
            chooseFileDialog.FilterIndex = 1;
            chooseFileDialog.Title = "Wybierz plik do zaszyfrowania";
            chooseFileDialog.Multiselect = false;

            if (chooseFileDialog.ShowDialog() == DialogResult.OK)
            {
                cryptSourceFileBox.Text = chooseFileDialog.FileName;
                messageBox.postMessage("Poprawnie wybrano plik źródłowy do szyfrowania");


            }
        }

        private void cryptButton_Click(object sender, EventArgs e)
        {
            if (this.cryptButton.Text == "Szyfruj")
            {

                if (areCryptBoxesFilled())
                {
                    backgroundWorker = new BackgroundWorker();

                    CryptographyParameters cryptographyParameters = new CryptographyParameters(backgroundWorker);
                    cryptographyParameters.AlgorithmName = "Twofish";
                    cryptographyParameters.forEncryption = true;
                    cryptographyParameters.Mode = this.cryptWorkModeBox.Text;
                    if (cryptographyParameters.Mode == "CFB" || cryptographyParameters.Mode == "OFB")
                    {
                        cryptographyParameters.SegmentSize = this.cryptBlockLengthBox.Text;
                    }
                    cryptographyParameters.KeyLength = this.cryptKeyLengthBox.Text;
                    cryptographyParameters.sourceFilePathWithFilename = this.cryptSourceFileBox.Text;
                    cryptographyParameters.destinationFilePathWithFilename = this.cryptDestinationFileBox.Text;
                    List<string> chosenUsers = this.chosenBoxCrypt.Items.Cast<String>().ToList();
                    List<PublicUser> publicUsersForEncryption = getPublicUsers(chosenUsers);
                    foreach (PublicUser x in publicUsersForEncryption)
                    {
                        cryptographyParameters.setApprovedUser(x.EmailOfUser, x.PublicKeyOfUser);
                    }

                    backgroundWorker.WorkerReportsProgress = true;
                    backgroundWorker.WorkerSupportsCancellation = true;
                    backgroundWorker.DoWork += new DoWorkEventHandler(Cryptograph.encryptFile);
                    backgroundWorker.RunWorkerCompleted += encryptFileDoneAsync;
                    backgroundWorker.ProgressChanged += progressChangedAsync;
                    this.cryptButton.Text = "Anuluj";
                    this.decryptButton.Enabled = false;
                    messageBox.postMessage("Szyfrowanie...");
                    backgroundWorker.RunWorkerAsync(cryptographyParameters);
                }
                else
                {
                    messageBox.postMessage("Proces szyfrowania nie uruchomił się");
                }

            }
            else
            {
                this.cryptButton.Enabled = false;
                messageBox.postMessage("Anulowanie...");
                backgroundWorker.CancelAsync();
            }



        }

        

        private bool areCryptBoxesFilled()
        {
            bool isGood = true;
            StringBuilder str = new StringBuilder();
            if (String.IsNullOrWhiteSpace(this.cryptWorkModeBox.Text))
            {
                str.Append("Nie wybrałeś trybu blokowego szyfrowania sposród dostępnych. ");
                isGood = false;
            }
            else
            {
                if (this.cryptWorkModeBox.Text != "ECB" && this.cryptWorkModeBox.Text != "CBC")
                {
                    if (String.IsNullOrWhiteSpace(this.cryptBlockLengthBox.Text))
                    {
                        isGood = false;
                        str.Append("Nie wybrałeś długości podbloku dla wybranego trybu. ");
                    }
                }
            }
            if (String.IsNullOrWhiteSpace(this.cryptKeyLengthBox.Text))
            {
                isGood = false;
                str.Append("Nie wybrałeś długości klucza. ");
            }
            if (String.IsNullOrWhiteSpace(this.cryptSourceFileBox.Text))
            {
                isGood = false;
                str.Append("Nie wybrałeś pliku do szyfrowania. ");
            }
            else
            {
                if (!File.Exists(this.cryptSourceFileBox.Text))
                {
                    isGood = false;
                    str.Append("Plik źródłowy nie istnieje. ");
                }
            }
            if (String.IsNullOrWhiteSpace(this.cryptDestinationFileBox.Text))
            {
                isGood = false;
                str.Append("Nie wybrałeś miejsca i nazwę docelowego, zaszyfrowanego pliku. ");
            }
            if (this.cryptSourceFileBox.Text == this.cryptDestinationFileBox.Text)
            {
                isGood = false;
                str.Append("Plik źródłowy i docelowy jest taki sam! Podaj inną nazwę pliku docelowego. ");
            }
            if (this.chosenBoxCrypt.Items.Count <= 0)
            {
                isGood = false;
                str.Append("Nie wybrałeś docelowych użytkowników. ");
            }
            if (isGood == false)
            {
                this.messageBox.postMessageAlert(str.ToString());
                return false;
            }
            else return true;

        }


        private List<PublicUser> getPublicUsers(List<string> chosenUsers)
        {
            List<PublicUser> pomList = new List<PublicUser>();
            foreach (string c in chosenUsers)
            {
                var returnValue = this.publicUsers.FirstOrDefault((x) => x.EmailOfUser == c);
                if (returnValue != null)
                {
                    pomList.Add(returnValue);
                }
            }
            return pomList;
        }

        private PrivateUser getPrivateUser(string chosenUser)
        {
            var privateUser = privateUsers.FirstOrDefault(n => n.EmailOfUser == chosenUser);

            return privateUser;
        }

        private void encryptFileDoneAsync(object sender, RunWorkerCompletedEventArgs e)
        {

            if (e.Cancelled)
            {
                messageBox.postMessageAsync("Szyfrowanie przerwane.");
            }
            else
            {
                string result = (string)e.Result;
                messageBox.postMessageAsync("Szyfrowanie wykonane. " + result);
            }
            setButtonEnabledAsync(this.decryptButton, true);
            setButtonTextAsync(this.cryptButton, "Szyfruj");
            setButtonEnabledAsync(this.cryptButton, true);
            setProgessBarAsync(this.progressBar1, 0);

        }


        private void cryptKeyLengthBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //change 120 value
         //   int value = Int32.Parse((string)this.cryptKeyLengthBox.SelectedItem);
            if (this.cryptBlockLengthBox.SelectedItem == null)
            {
                clearAndSetBlockLengthValues(120);
            }
            else if (120 > Int32.Parse((string)this.cryptBlockLengthBox.SelectedItem))
            {
                clearAndSetBlockLengthValues(120);
            }
            else if (120 < Int32.Parse((string)this.cryptBlockLengthBox.SelectedItem))
            {
                clearAndSetBlockLengthValues(120);
                
            }
            


        }

        private void clearAndSetBlockLengthValues(int value)
        {
            this.cryptBlockLengthBox.Items.Clear();
            for (int i = 8; i <= value; i += 8)
            {
                this.cryptBlockLengthBox.Items.Add(i.ToString());
            }
        }

        private void cryptWorkModeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string)this.cryptWorkModeBox.SelectedItem == "OFB" || (string)this.cryptWorkModeBox.SelectedItem == "CFB")
            {
                this.cryptBlockLengthBox.Enabled = true;
            }
            else
            {
                this.cryptBlockLengthBox.Enabled = false;
            }
        }



        private void cryptAddUserButton_Click(object sender, EventArgs e)
        {
            List<string> itemsToRemove = new List<string>(this.availableBoxCrypt.SelectedItems.Cast<string>().ToList());
            foreach (string x in itemsToRemove)
            {
                this.chosenBoxCrypt.Items.Add(x);
                this.availableBoxCrypt.Items.Remove(x);
            }


        }

        private void cryptRemoveUserButton_Click(object sender, EventArgs e)
        {
            List<string> itemsToRemove = new List<string>(this.chosenBoxCrypt.SelectedItems.Cast<string>().ToList());
            foreach (String x in itemsToRemove)
            {
                this.chosenBoxCrypt.Items.Remove(x);
                this.availableBoxCrypt.Items.Add(x);
            }

        }
    }
}
