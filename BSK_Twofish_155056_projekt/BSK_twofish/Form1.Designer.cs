﻿
namespace BSK_twofish
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        internal System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        internal void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cryptButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cryptCreateNewUserButton = new System.Windows.Forms.Button();
            this.cryptRemoveUserButton = new System.Windows.Forms.Button();
            this.cryptAddUserButton = new System.Windows.Forms.Button();
            this.chosenBoxCrypt = new System.Windows.Forms.ListBox();
            this.availableBoxCrypt = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cryptBlockLengthBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cryptWorkModeBox = new System.Windows.Forms.ComboBox();
            this.cryptKeyLengthBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cryptDestinationFileButton = new System.Windows.Forms.Button();
            this.cryptSourceFileButton = new System.Windows.Forms.Button();
            this.cryptDestinationFileBox = new System.Windows.Forms.TextBox();
            this.cryptSourceFileBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.decryptButton = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.availableBoxDecrypt = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.decryptPasswordTextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.decryptDestinationFileButton = new System.Windows.Forms.Button();
            this.decryptSourceFileButton = new System.Windows.Forms.Button();
            this.decryptDestinationFileBox = new System.Windows.Forms.TextBox();
            this.decryptSourceFileBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.messageBox = new BSK_twofish.MessageBoxForm();
            this.decryptLoadUsersButton = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(581, 410);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(573, 384);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Szyfrator";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cryptButton);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.cryptCreateNewUserButton);
            this.groupBox3.Controls.Add(this.cryptRemoveUserButton);
            this.groupBox3.Controls.Add(this.cryptAddUserButton);
            this.groupBox3.Controls.Add(this.chosenBoxCrypt);
            this.groupBox3.Controls.Add(this.availableBoxCrypt);
            this.groupBox3.Location = new System.Drawing.Point(3, 202);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(561, 176);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Odbiorcy";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // cryptButton
            // 
            this.cryptButton.Location = new System.Drawing.Point(460, 147);
            this.cryptButton.Name = "cryptButton";
            this.cryptButton.Size = new System.Drawing.Size(95, 23);
            this.cryptButton.TabIndex = 0;
            this.cryptButton.Text = "Szyfruj";
            this.cryptButton.UseVisualStyleBackColor = true;
            this.cryptButton.Click += new System.EventHandler(this.cryptButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(352, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Wybrani";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Dostępni";
            // 
            // cryptCreateNewUserButton
            // 
            this.cryptCreateNewUserButton.Location = new System.Drawing.Point(233, 102);
            this.cryptCreateNewUserButton.Name = "cryptCreateNewUserButton";
            this.cryptCreateNewUserButton.Size = new System.Drawing.Size(95, 23);
            this.cryptCreateNewUserButton.TabIndex = 3;
            this.cryptCreateNewUserButton.Text = "Stwórz Odbiorcę";
            this.cryptCreateNewUserButton.UseVisualStyleBackColor = true;
            this.cryptCreateNewUserButton.Click += new System.EventHandler(this.cryptCreateNewUserButtonClick);
            // 
            // cryptRemoveUserButton
            // 
            this.cryptRemoveUserButton.Location = new System.Drawing.Point(233, 73);
            this.cryptRemoveUserButton.Name = "cryptRemoveUserButton";
            this.cryptRemoveUserButton.Size = new System.Drawing.Size(95, 23);
            this.cryptRemoveUserButton.TabIndex = 2;
            this.cryptRemoveUserButton.Text = "<- Usuń";
            this.cryptRemoveUserButton.UseVisualStyleBackColor = true;
            this.cryptRemoveUserButton.Click += new System.EventHandler(this.cryptRemoveUserButton_Click);
            // 
            // cryptAddUserButton
            // 
            this.cryptAddUserButton.Location = new System.Drawing.Point(233, 44);
            this.cryptAddUserButton.Name = "cryptAddUserButton";
            this.cryptAddUserButton.Size = new System.Drawing.Size(95, 23);
            this.cryptAddUserButton.TabIndex = 1;
            this.cryptAddUserButton.Text = "Dodaj ->";
            this.cryptAddUserButton.UseVisualStyleBackColor = true;
            this.cryptAddUserButton.Click += new System.EventHandler(this.cryptAddUserButton_Click);
            // 
            // chosenBoxCrypt
            // 
            this.chosenBoxCrypt.FormattingEnabled = true;
            this.chosenBoxCrypt.Location = new System.Drawing.Point(355, 40);
            this.chosenBoxCrypt.Name = "chosenBoxCrypt";
            this.chosenBoxCrypt.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.chosenBoxCrypt.Size = new System.Drawing.Size(200, 95);
            this.chosenBoxCrypt.TabIndex = 4;
            // 
            // availableBoxCrypt
            // 
            this.availableBoxCrypt.FormattingEnabled = true;
            this.availableBoxCrypt.Location = new System.Drawing.Point(6, 40);
            this.availableBoxCrypt.Name = "availableBoxCrypt";
            this.availableBoxCrypt.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.availableBoxCrypt.Size = new System.Drawing.Size(200, 95);
            this.availableBoxCrypt.TabIndex = 0;
            this.availableBoxCrypt.SelectedIndexChanged += new System.EventHandler(this.Dostępni_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cryptBlockLengthBox);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cryptWorkModeBox);
            this.groupBox2.Controls.Add(this.cryptKeyLengthBox);
            this.groupBox2.Location = new System.Drawing.Point(6, 114);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(561, 82);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ustawienia";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(280, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Długość podbloku:";
            // 
            // cryptBlockLengthBox
            // 
            this.cryptBlockLengthBox.FormattingEnabled = true;
            this.cryptBlockLengthBox.Location = new System.Drawing.Point(403, 46);
            this.cryptBlockLengthBox.Name = "cryptBlockLengthBox";
            this.cryptBlockLengthBox.Size = new System.Drawing.Size(121, 21);
            this.cryptBlockLengthBox.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Tryb pracy:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Długość klucza:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // cryptWorkModeBox
            // 
            this.cryptWorkModeBox.FormattingEnabled = true;
            this.cryptWorkModeBox.Items.AddRange(new object[] {
            "ECB",
            "CBC",
            "CFB",
            "OFB"});
            this.cryptWorkModeBox.Location = new System.Drawing.Point(403, 19);
            this.cryptWorkModeBox.Name = "cryptWorkModeBox";
            this.cryptWorkModeBox.Size = new System.Drawing.Size(121, 21);
            this.cryptWorkModeBox.TabIndex = 1;
            this.cryptWorkModeBox.SelectedIndexChanged += new System.EventHandler(this.cryptWorkModeBox_SelectedIndexChanged);
            // 
            // cryptKeyLengthBox
            // 
            this.cryptKeyLengthBox.FormattingEnabled = true;
            this.cryptKeyLengthBox.Items.AddRange(new object[] {
            "128",
            "192",
            "256"});
            this.cryptKeyLengthBox.Location = new System.Drawing.Point(117, 19);
            this.cryptKeyLengthBox.Name = "cryptKeyLengthBox";
            this.cryptKeyLengthBox.Size = new System.Drawing.Size(121, 21);
            this.cryptKeyLengthBox.TabIndex = 0;
            this.cryptKeyLengthBox.SelectedIndexChanged += new System.EventHandler(this.cryptKeyLengthBox_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cryptDestinationFileButton);
            this.groupBox1.Controls.Add(this.cryptSourceFileButton);
            this.groupBox1.Controls.Add(this.cryptDestinationFileBox);
            this.groupBox1.Controls.Add(this.cryptSourceFileBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(561, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pliki";
            // 
            // cryptDestinationFileButton
            // 
            this.cryptDestinationFileButton.Location = new System.Drawing.Point(449, 49);
            this.cryptDestinationFileButton.Name = "cryptDestinationFileButton";
            this.cryptDestinationFileButton.Size = new System.Drawing.Size(75, 23);
            this.cryptDestinationFileButton.TabIndex = 3;
            this.cryptDestinationFileButton.Text = "Wybierz";
            this.cryptDestinationFileButton.UseVisualStyleBackColor = true;
            this.cryptDestinationFileButton.Click += new System.EventHandler(this.cryptDestinationFileButton_Click);
            // 
            // cryptSourceFileButton
            // 
            this.cryptSourceFileButton.Location = new System.Drawing.Point(449, 13);
            this.cryptSourceFileButton.Name = "cryptSourceFileButton";
            this.cryptSourceFileButton.Size = new System.Drawing.Size(75, 23);
            this.cryptSourceFileButton.TabIndex = 1;
            this.cryptSourceFileButton.Text = "Wybierz";
            this.cryptSourceFileButton.UseVisualStyleBackColor = true;
            this.cryptSourceFileButton.Click += new System.EventHandler(this.cryptChooseSourceFileButtonClick);
            // 
            // cryptDestinationFileBox
            // 
            this.cryptDestinationFileBox.Location = new System.Drawing.Point(117, 51);
            this.cryptDestinationFileBox.Name = "cryptDestinationFileBox";
            this.cryptDestinationFileBox.Size = new System.Drawing.Size(326, 20);
            this.cryptDestinationFileBox.TabIndex = 2;
            // 
            // cryptSourceFileBox
            // 
            this.cryptSourceFileBox.Location = new System.Drawing.Point(117, 13);
            this.cryptSourceFileBox.Name = "cryptSourceFileBox";
            this.cryptSourceFileBox.Size = new System.Drawing.Size(326, 20);
            this.cryptSourceFileBox.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Plik wyjściowy:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Plik wejściowy:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.decryptButton);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(573, 384);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Deszyfrator";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // decryptButton
            // 
            this.decryptButton.Location = new System.Drawing.Point(463, 349);
            this.decryptButton.Name = "decryptButton";
            this.decryptButton.Size = new System.Drawing.Size(95, 23);
            this.decryptButton.TabIndex = 0;
            this.decryptButton.Text = "Deszyfruj";
            this.decryptButton.UseVisualStyleBackColor = true;
            this.decryptButton.Click += new System.EventHandler(this.decryptButton_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.decryptLoadUsersButton);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.availableBoxDecrypt);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.decryptPasswordTextBox);
            this.groupBox5.Location = new System.Drawing.Point(6, 114);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(561, 208);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Odbiorca";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(29, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(117, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Wybierz odbiorcę z listy";
            // 
            // availableBoxDecrypt
            // 
            this.availableBoxDecrypt.FormattingEnabled = true;
            this.availableBoxDecrypt.Location = new System.Drawing.Point(22, 50);
            this.availableBoxDecrypt.Name = "availableBoxDecrypt";
            this.availableBoxDecrypt.Size = new System.Drawing.Size(515, 95);
            this.availableBoxDecrypt.TabIndex = 0;
            this.availableBoxDecrypt.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Wpisz hasło:";
            // 
            // decryptPasswordTextBox
            // 
            this.decryptPasswordTextBox.Location = new System.Drawing.Point(136, 154);
            this.decryptPasswordTextBox.Name = "decryptPasswordTextBox";
            this.decryptPasswordTextBox.PasswordChar = '*';
            this.decryptPasswordTextBox.Size = new System.Drawing.Size(388, 20);
            this.decryptPasswordTextBox.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.decryptDestinationFileButton);
            this.groupBox4.Controls.Add(this.decryptSourceFileButton);
            this.groupBox4.Controls.Add(this.decryptDestinationFileBox);
            this.groupBox4.Controls.Add(this.decryptSourceFileBox);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(6, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(561, 101);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pliki";
            // 
            // decryptDestinationFileButton
            // 
            this.decryptDestinationFileButton.Location = new System.Drawing.Point(449, 49);
            this.decryptDestinationFileButton.Name = "decryptDestinationFileButton";
            this.decryptDestinationFileButton.Size = new System.Drawing.Size(75, 23);
            this.decryptDestinationFileButton.TabIndex = 3;
            this.decryptDestinationFileButton.Text = "Wybierz";
            this.decryptDestinationFileButton.UseVisualStyleBackColor = true;
            this.decryptDestinationFileButton.Click += new System.EventHandler(this.decryptDestinationFileButton_Click);
            // 
            // decryptSourceFileButton
            // 
            this.decryptSourceFileButton.Location = new System.Drawing.Point(449, 13);
            this.decryptSourceFileButton.Name = "decryptSourceFileButton";
            this.decryptSourceFileButton.Size = new System.Drawing.Size(75, 23);
            this.decryptSourceFileButton.TabIndex = 1;
            this.decryptSourceFileButton.Text = "Wybierz";
            this.decryptSourceFileButton.UseVisualStyleBackColor = true;
            this.decryptSourceFileButton.Click += new System.EventHandler(this.decryptSourceFileButton_Click);
            // 
            // decryptDestinationFileBox
            // 
            this.decryptDestinationFileBox.Location = new System.Drawing.Point(117, 51);
            this.decryptDestinationFileBox.Name = "decryptDestinationFileBox";
            this.decryptDestinationFileBox.Size = new System.Drawing.Size(326, 20);
            this.decryptDestinationFileBox.TabIndex = 2;
            // 
            // decryptSourceFileBox
            // 
            this.decryptSourceFileBox.Location = new System.Drawing.Point(117, 13);
            this.decryptSourceFileBox.Name = "decryptSourceFileBox";
            this.decryptSourceFileBox.Size = new System.Drawing.Size(326, 20);
            this.decryptSourceFileBox.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Plik wyjściowy:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Plik wejściowy:";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(16, 428);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(573, 23);
            this.progressBar1.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 464);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Komunikaty";
            // 
            // messageBox
            // 
            this.messageBox.Location = new System.Drawing.Point(16, 480);
            this.messageBox.Name = "messageBox";
            this.messageBox.ReadOnly = true;
            this.messageBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.messageBox.Size = new System.Drawing.Size(573, 78);
            this.messageBox.TabIndex = 0;
            this.messageBox.Text = "";
            this.messageBox.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // decryptLoadUsersButton
            // 
            this.decryptLoadUsersButton.Location = new System.Drawing.Point(417, 19);
            this.decryptLoadUsersButton.Name = "decryptLoadUsersButton";
            this.decryptLoadUsersButton.Size = new System.Drawing.Size(107, 23);
            this.decryptLoadUsersButton.TabIndex = 6;
            this.decryptLoadUsersButton.Text = "Załaduj Odbiorców";
            this.decryptLoadUsersButton.UseVisualStyleBackColor = true;
            this.decryptLoadUsersButton.Click += new System.EventHandler(this.decryptLoadUsersButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 570);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.messageBox);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "BSK Twofish";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TabControl tabControl1;
        internal System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.TextBox cryptDestinationFileBox;
        internal System.Windows.Forms.TextBox cryptSourceFileBox;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TabPage tabPage2;
        internal System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.ComboBox cryptWorkModeBox;
        internal System.Windows.Forms.ComboBox cryptKeyLengthBox;
        internal System.Windows.Forms.Button cryptDestinationFileButton;
        internal System.Windows.Forms.Button cryptSourceFileButton;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.ComboBox cryptBlockLengthBox;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Button cryptRemoveUserButton;
        internal System.Windows.Forms.Button cryptAddUserButton;
        internal System.Windows.Forms.ListBox chosenBoxCrypt;
        internal System.Windows.Forms.ListBox availableBoxCrypt;
        internal System.Windows.Forms.Button cryptCreateNewUserButton;
        internal System.Windows.Forms.Button cryptButton;
        internal MessageBoxForm messageBox;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.ProgressBar progressBar1;
        internal System.Windows.Forms.Button decryptButton;
        internal System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.ListBox availableBoxDecrypt;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.TextBox decryptPasswordTextBox;
        internal System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.Button decryptDestinationFileButton;
        internal System.Windows.Forms.Button decryptSourceFileButton;
        internal System.Windows.Forms.TextBox decryptDestinationFileBox;
        internal System.Windows.Forms.TextBox decryptSourceFileBox;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button decryptLoadUsersButton;
    }
}

