# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Project for studies 06/2017
This program crypt files using Twofish.
Block cipher mode of operation: CFB, CBC, OFB, ECB.
Size of key 128, 192, 256.
It can crypt one file to many recipents - by coding session key with public RSA key of users.

All users should keep their private encrypted with password in safe place - like usb or cd.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

Places to improve:

Better GUI.
Keys of users should be able to load from any location not only from program - by that way user could load private key from usb - not by copying to app folder.

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact